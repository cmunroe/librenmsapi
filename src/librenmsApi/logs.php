<?php

namespace librenmsApi;

class logs extends core {

    /**
     * List Eventlog
     *
     * @param string $hostname id or hostname is the specific device
     * @param array $option
     * - start The page number to request.
     * - limit The limit of results to be returned.
     * - from The date and time to search from.
     * - to The data and time to search to.
     * @return object
     */
    public function list_eventlog(string $hostname = null, array $options = array()){

        return json_decode($this->call_api('logs/eventlog/' . $hostname . '?' . http_build_query($options)));

    }

    /**
     * List Syslog
     *
     * @param string $hostname id or hostname is the specific device
     * @param array $option
     * - start The page number to request.
     * - limit The limit of results to be returned.
     * - from The date and time to search from.
     * - to The data and time to search to.
     * @return object
     */
    public function list_syslog(string $hostname = null, array $options = array()){

        return json_decode($this->call_api('logs/syslog/' . $hostname . '?' . http_build_query($options)));

    }

    /**
     * List Alert Log
     *
     * @param string $hostname id or hostname is the specific device
     * @param array $option
     * - start The page number to request.
     * - limit The limit of results to be returned.
     * - from The date and time to search from.
     * - to The data and time to search to.
     * @return object
     */
    public function list_alertlog(string $hostname = null, array $options = array()){

        return json_decode($this->call_api('logs/alertlog/' . $hostname . '?' . http_build_query($options)));

    }

    /**
     * List Authlog
     *
     * @param string $hostname id or hostname is the specific device
     * @param array $option
     * - start The page number to request.
     * - limit The limit of results to be returned.
     * - from The date and time to search from.
     * - to The data and time to search to.
     * @return object
     */
    public function list_authlog(string $hostname = null, array $options = array()){

        return json_decode($this->call_api('logs/authlog/' . $hostname . '?' . http_build_query($options)));

    }

}