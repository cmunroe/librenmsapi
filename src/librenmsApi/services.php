<?php

namespace librenmsApi;

class services extends core {

    /**
     * Retrieve all services
     *
     * @param integer $state only which have a certain state (valid options are 0=Ok, 1=Warning, 2=Critical).
     * @param string $type service type, used sql LIKE to find services, so for tcp, use type=tcp for http use type=http
     * @return object
     */
    public function list_services(int $state = null, string $type = null){

        return json_decode($this->call_api('services?state=' . $state . '&type=' . $type));
        
    }

    /**
     * Retrieve services for device
     *
     * @param string $hostname id or hostname is the specific device
     * @param integer $state only which have a certain state (valid options are 0=Ok, 1=Warning, 2=Critical).
     * @param string $type service type, used sql LIKE to find services, so for tcp, use type=tcp for http use type=http
     * @return object
     */
    public function get_service_for_host(string $hostname, int $state = null, string $type = null){

        return json_decode($this->call_api('services/' . $hostname . '?state=' . $state . '&type=' . $type));

    }

    /**
     * Add a service for device
     *
     * @param string $hostname id or hostname is the specific device
     * @param array $options
     * - [type]  service type
     * - [ip]  ip of the service
     * - [desc] description for the service
     * - [param] parameters for the service
     * = [ignore]  ignore the service for checks
     * @return object
     */
    public function add_service_for_host(string $hostname, array $options = array()){

        // $options = array(

        //     'type' => $type,
        //     'ip' => $ip,
        //     'desc' => $desc,
        //     'param' => $param,
        //     'ignore' => $ignore

        // );

        $options = json_encode($options);

        return json_decode($this->call_api('services/' . $hostname, 'POST', $options));

    }

}