<?php

namespace librenmsApi;

class routing extends core {

    /**
     * List the current BGP sessions.
     *
     * @param string $hostname Either the devices hostname or id.
     * @param integer $asn The ASN you would like to filter by
     * @return object
     */
    public function list_bgp(string $hostname = null, int $asn = null){

        return json_decode($this->call_api('bgp?hostname=' . $hostname . '&asn=' . $asn));

    }

    /**
     * Retrieves a BGP session by ID
     *
     * @param integer $id BGP Session ID.
     * @return object
     */
    public function get_bgp(int $id){

        return json_decode($this->call_api('bgp/' . $id));

    }

    /**
     * List the current BGP sessions counters.
     *
     * @param string $hostname  Either the devices hostname or id
     * @return object
     */
    public function list_cbgp(string $hostname = null){

        return json_decode($this->call_api('routing/bgp/cbgp?hostname=' . $hostname));

    }

    /**
     * List all IPv4 and IPv6 addresses.
     *
     * @return object
     */
    public function list_ip_addresses(){

        return json_decode($this->call_api('resources/ip/addresses'));

    }

    /**
     * Get all IPv4 and IPv6 addresses for particular network.
     *
     * @param integer $id must be integer
     * @return object
     */
    public function get_network_ip_addresses(int $id){

        return json_decode($this->call_api('resources/ip/networks/' . $id . '/ip'));

    }

    /**
     * List all IPv4 and IPv6 networks.
     *
     * @return object
     */
    public function list_ip_networks(){

        return json_decode($this->call_api('resources/ip/networks/'));

    }

    /**
     * List the current IPSec tunnels which are active.
     *
     * @param string $hostname can be either the device hostname or id
     * @return object 
     */
    public function list_ipsec(string $hostname = null){

        return json_decode($this->call_api('routing/ipsec/data/' . $hostname));

    }  

    /**
     * List the current OSPF neighbours.
     *
     * @param string $hostname
     * @return void
     */
    public function list_ospf(string $hostname = null){

        return json_decode($this->call_api('ospf?hostname=' . $hostname));

    }

    /**
     * List the current VRFs.
     *
     * @param array $options ::
     *  - [hostname] Either the devices hostname or id (only one paramater can be set).
     *  - [vrfname] The VRF name you would like to filter by (only one paramater can be set).
     * @return object
     */
    public function list_vrf(array $options = array()){

        return json_decode($this->call_api('routing/vrf?' . http_build_query($options)));

    }

    /**
     * Retrieves VRF by ID
     *
     * @param integer $id VRF ID. 
     * @return object
     */
    public function get_vrf(int $id){

        return json_decode($this->call_api('routing/vrf/'. $id));

    }

}