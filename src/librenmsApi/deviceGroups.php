<?php

namespace librenmsApi;

class deviceGroups extends core {

    

    /**
     * Get Device Group Data
     *
     * @param string $group 
     * @return object 
     */
    public function get_devices_by_group($group){

        return json_decode($this->call_api("devicegroups/" . $group));
        
    }

    /**
     * Get a list of device groups on LibreNMS
     *
     * @return object returns a list of groups.
     */
    public function get_devicegroups(){
        
        return json_decode($this->call_api("devicegroups/"));

    }

}