<?php

namespace librenmsApi;

class ports extends core {

    /**
     * Get info for all ports on all devices. Strongly recommend that you use the columns parameter to avoid pulling too much data.
     *
     * @param string $columns Comma separated list of columns you want returned.
     * @return object
     */
    public function get_all_ports(string $columns = null){

        return json_decode($this->call_api('ports?columns=' . $columns));

    }

    /**
     * Get all info for a particular port.
     *
     * @param integer $portid must be an integer
     * @return object
     */
    public function get_port_info(int $portid){

        return json_decode($this->call_api('ports/' . $portid));

    }

    /**
     * Get all IP info (v4 and v6) for a given port id.
     *
     * @param integer $portid must be an integer
     * @return object
     */
    public function get_port_ip_info(int $portid){

        return json_decode($this->call_api('ports/' . $portid . '/ip'));

    }

}