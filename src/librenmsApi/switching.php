<?php

namespace librenmsApi;

class switching extends core {
    
    /**
     * Get a list of all VLANs.
     *
     * @return object list of vlans.
     */
    public function list_vlans(){

        return json_decode($this->call_api('resources/vlans'), true);

    }

    /**
     * Get a list of all VLANs for a given device.
     *
     * @param string $hostname hostname can be either the device hostname or id
     * @return object list of vlans.
     */
    public function get_vlans(string $hostname){

        return json_decode($this->call_api('devices/' . $hostname . '/vlans'));

    }

    /**
     * Get a list of all Links.
     *
     * @return object
     */
    public function list_links(){

        return json_decode($this->call_api('resources/links'), true);

    }

    /**
     * Get a list of Links per giver device.
     *
     * @param string $hostname hostname can be either the device hostname or id
     * @return object
     */
    public function get_links(string $hostname){

        return json_decode($this->call_api('devices/' . $hostname . '/links'));

    }

    /**
     * Retrieves Link by ID
     *
     * @param integer $linkID Link ID.
     * @return object
     */
    public function get_link(int $linkID){

        return json_decode($this->call_api('resources/link/' . $linkID));

    }

    /**
     * Get a list of all ports FDB.
     *
     * @param string $mac mac is the specific MAC address you would like to query. Can be null.
     * @return object
     */
    public function list_fdb(string $mac = null){

        // Remove :,-, and spaces.
        if(isset($mac)){

            $mac = str_replace(':', '', $mac);
            $mac = str_replace('-', '', $mac);
            $mac = str_replace(' ', '', $mac);

        }

        return json_decode($this->call_api('resources/fdb/' . $mac));

    }  

}