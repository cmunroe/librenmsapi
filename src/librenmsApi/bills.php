<?php

namespace librenmsApi;

/**
 * LibreNMS Billing Class
 * 
 * https://docs.librenms.org/API/Bills/
 */
class bills extends core {

    /**
     * Retrieve the list of bills currently in the system.
     *
     * @param string $period previous
     * @return object
     */
    public function list_bills(string $period = null){

        return json_decode($this->call_api("bills?period=" . $period));
        
    }

    /**
     * Retrieve a specific bill
     *
     * @param integer $id is the specific bill id
     * @param array $options
     * ref is the billing reference
     * custid is the customer reference
     * period=previous indicates you would like the data for the last complete period rather than the current period
     * @return void
     */
    public function get_bill(int $id = null, array $options = array()){

        return json_decode($this->call_api("bills/" . $id . "?" . http_build_query($options)));

    }

    /**
     * Retrieve a graph image associated with a bill.
     *
     * @param integer $id is the specific bill id
     * @param string $type bits/monthly
     * @param array $option
     * from: unixtime
     * to: unixtime
     * @return string image
     */
    public function get_bill_graph(int $id, string $type, array $option = array()){

        return json_decode($this->call_api("bills/" . $id . "/graphs/" . $type . http_build_query($options)));

    }

    /**
     * Retrieve the data used to draw a graph so it can be rendered in an external system
     *
     * @param integer $id is the specific bill id
     * @param string $type bits/monthly
     * @param array $option
     * from: unixtime
     * to: unixtime
     * reducefactor: int
     *  - The reducefactor parameter is used to reduce the number of data points.
     *  - Billing data has 5 minute granularity, so requesting a graph for a long time period will result in many data points.
     *  - If not supplied, it will be automatically calculated. A reducefactor of 1 means return all items, 2 means half of the items etc.
     * @return object
     */
    public function get_bill_graphdata(int $id, string $type, array $option = array()){

        return json_decode($this->call_api("bills/" . $id . "/graphdata/" . $type . http_build_query($options)));

    }

    /**
     * Retrieve the history of specific bill
     *
     * @param integer $id is the specific bill id
     * @return object
     */
    public function get_bill_history(int $id){

        return json_decode($this->call_api("bills/" . $id . "/history"));

    }

    /**
     * Retrieve a graph of a previous period of a bill
     * 
     * NB: The graphs returned from this will always be png as they do not come from rrdtool, even if you have SVG set.
     *
     * @param integer $id is the specific bill id
     * @param integer $bill_hist_id 
     * @param string $graph_type day / hour /bits
     * @return string image
     */
    public function get_bill_history_graph(int $id, int $bill_hist_id, string $graph_type){

        return json_decode($this->call_api("bills/" . $id . "/history/" . $bill_hist_id . '/graphs/' . $graph_type));

    }

    /**
     * Retrieve the data for a graph of a previous period of a bill, to be rendered in an external system
     * 
     *
     * @param integer $id is the specific bill id
     * @param integer $bill_hist_id 
     * @param string $graph_type day / hour /bits
     * @return string image
     */
    public function get_bill_history_graphdata(int $id, int $bill_hist_id, string $graph_type){

        return json_decode($this->call_api("bills/" . $id . "/history/" . $bill_hist_id . '/graphdata/' . $graph_type));

    }

    /**
     * Delete a specific bill and all dependent data
     *
     * @param integer $id is the specific bill id
     * @return object
     */
    public function delete_bill(int $id){

        return json_decode($this->call_api("bills/" . $id, 'DELETE'));

    }

    /**
     * Creates a new bill or updates an existing one
     *
     * @param array $edits
     * '{
     *      "bill_id":"32",
     *      "ports":[ 1021 ],
     *      "bill_name":"NEWNAME",
     *      "bill_quota":"1000000000000"
     * }'
     * '{   
     *      "ports":[ 1021 ],
     *      "bill_name":"NEWBILL",
     *      "bill_day":"1",
     *      "bill_type":"quota",
     *      "bill_quota":"2000000000000",
     *      "bill_custid":"1337",
     *      "bill_ref":"reference1",
     *      "bill_notes":"mynote"
     * }'
     * 
     * @return object 
     */
    public function create_edit_bill(array $edits = array()){

        $edits = json_encode($edits);

        return json_decode($this->call_api("bills", 'POST', $edits));

    }

}