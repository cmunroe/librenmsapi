<?php

namespace librenmsApi;

class alerts extends core {

    /**
     * Get details of an alert
     *
     * @param integer $id is the alert id, you can obtain a list of alert ids from list_alerts.
     * @return object
     */
    public function get_alert(int $id){

        return json_decode($this->call_api('alerts/' . $id));

    }

    /**
     * Acknowledge an alert
     *
     * @param integer $id id is the alert id, you can obtain a list of alert ids from list_alerts.
     * @param string $note  is the note to add to the alert
     * @param boolean $until_clear is a boolean and if set to false, the alert will re-alert if it worsens/betters.
     * @return object
     */
    public function ack_alert(int $id, string $note = null, bool $until_clear = false){

        $content = array(
            'note' => $note,
            'until_clear' => $until_clear
        );

        $content = json_encode($content);

        return json_decode($this->call_api('alerts/' . $id, 'PUT', $content));

    }


    /**
     * Unmute an alert
     *
     * @param integer $id id is the alert id, you can obtain a list of alert ids from list_alerts.
     * @return object
     */
    public function unmute_alert(int $id){

        return json_decode($this->call_api('alerts/unmute/' . $id, 'PUT'));

    }

    /**
     * List all alerts
     *
     * @param integer $state Filter the alerts by state, 0 = ok, 1 = alert, 2 = ack
     * @param string $severity Filter the alerts by severity. Valid values are ok, warning, critical.
     * @param string $order How to order the output, default is by timestamp (descending). Can be appended by DESC or ASC to change the order.
     * @return object
     */
    public function list_alerts(int $state = null, string $severity = null, string $order = null){

        return json_decode($this->call_api('alerts/?state=' . $state . '&severity=' . $severity . '$order=' . $order));

    }

    /**
     * Get the alert rule details.
     *
     * @param integer $id is the rule id.
     * @return object
     */
    public function get_alert_rule(int $id){

        return json_decode($this->call_api('rules/' . $id));

    }

    /**
     * Delete an alert rule by id
     *
     * @param integer $id id is the rule id.
     * @return object
     */
    public function delete_rule(int $id){

        return json_decode($this->call_api('rules/' . $id, 'DELETE'));

    }

    /**
     * List the alert rules.
     *
     * @return object
     */
    public function list_alert_rules(){

        return json_decode($this->call_api('rules'));

    }

    /**
     * Add a new alert rule.
     *
     * @param array $options ::
     * - string $name This is the name of the rule and is mandatory.
     * - string $builder The rule which should be in the format entity.condition value
     * (i.e devices.status != 0 for devices marked as down).
     * It must be json encoded in the format rules are currently stored.
     * - integer $devices This is either an array of device ids or -1 for a global rule
     * - string $severity The severity level the alert will be raised against, Ok, Warning, Critical.
     * - integer $disabled Whether the rule will be disabled or not, 0 = enabled, 1 = disabled
     * - integer $count This is how many polling runs before an alert will trigger and the frequency.
     * - string $delay Delay is when to start alerting and how frequently. The value is stored in seconds but you can specify minutes, hours or days by doing 5 m, 5 h, 5 d for each one.
     * - boolean $mute If mute is enabled then an alert will never be sent but will show up in the Web UI (true or false).
     * - boolean $invert This would invert the rules check.
     *   // $options = array(
     *   //     'name' => $name,
     *   //     'builder' => $builder,
     *   //     'devices' => $devices,
     *   //     'severity' => $severity,
     *   //     'disabled' => $disabled,
     *   //     'count' => $count,
     *   //     'delay' => $delay,
     *   //     'mute' => $mute,
     *   //     'invert' => $invert
     *   // );
     * @return object 
     */
    public function add_rule(array $options = array()){

        $options = json_encode($options);

        return json_decode($this->call_api('rules', 'POST', $options));

    }

    /**
     * Edit an existing alert rule
     *
     * @param array $options ::
     * - string $name This is the name of the rule and is mandatory.
     * - int $rule_id this is the rule you wish to edit. ((Mandatory))
     * - string $builder The rule which should be in the format entity.condition value
     * (i.e devices.status != 0 for devices marked as down).
     * It must be json encoded in the format rules are currently stored.
     * - integer $devices This is either an array of device ids or -1 for a global rule
     * - string $severity The severity level the alert will be raised against, Ok, Warning, Critical.
     * - integer $disabled Whether the rule will be disabled or not, 0 = enabled, 1 = disabled
     * - integer $count This is how many polling runs before an alert will trigger and the frequency.
     * - string $delay Delay is when to start alerting and how frequently. The value is stored in seconds but you can specify minutes, hours or days by doing 5 m, 5 h, 5 d for each one.
     * - boolean $mute If mute is enabled then an alert will never be sent but will show up in the Web UI (true or false).
     * - boolean $invert This would invert the rules check.
     *   // $options = array(
     *   //     'name' => $name,
     *   //     'rule_id' => $rule_id,
     *   //     'builder' => $builder,
     *   //     'devices' => $devices,
     *   //     'severity' => $severity,
     *   //     'disabled' => $disabled,
     *   //     'count' => $count,
     *   //     'delay' => $delay,
     *   //     'mute' => $mute,
     *   //     'invert' => $invert
     *   // );
     * @return object 
     */
    public function edit_rule(array $options = array()){

        $options = json_encode($options);

        return json_decode($this->call_api('rules', 'PUT', $options));

    }

}