<?php
/**
 * An API for connecting to LibreNMS, and querying the v0 api.
 */
namespace librenmsApi;

class core{

    /**
     * libreNMS API Token and Site.
     */
    protected $token = null;
    protected $site = null;

    /**
     * Load a NMS API Site and Token to query.
     *
     * @param string $site The URL to our API. https://mon.example.local
     * @param [type] $token The Token for the aboves API. 
     * @return this
     */
    public function set_api($site, $token){

        // Set token and site.
        $this->site = $site;
        $this->token = $token;

        // chain me!
        return $this;

    }
    
    /**
     * Main API Caller function that grabs the data from the api.
     *
     * @param string $path
     * @param string $method
     * @param string $content
     * @return string
     */
    public function call_api(string $path, string $method = 'GET', string $content = null){

        if(!isset($this->site) || !isset($this->token)){

            exit("API Token or Site not set.");

        }

        // Create our http Options.
        $options = array(
            'http'=>array(
                'method'=> $method,
                'content' => $content,
                'header'=>"X-Auth-Token: " . $this->token
            )
        );

        // Set our context.
        $context = stream_context_create($options);

        // Return our response
        return file_get_contents($this->site . "/api/v0/" . $path, false, $context);

    }


}