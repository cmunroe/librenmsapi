<?php

namespace librenmsApi;

class system extends core {

    /**
     * Get the system information of the LibreNMS install. Such as:
     * PHP Version
     * DB Schema
     * MySQL Version
     * et cetera.
     *
     * @return object returns system information in array.
     */
    public function system(){

        return json_decode($this->call_api('system'));

    }

}