<?php

namespace librenmsApi;

class arp extends core {

    /**
     * list ARP addresses by ip or cidr or everything in the database.
     *
     * @param string $search Search for a specific arp address by IP, or cidr.
     * @return object returns an arrray of arp entries.
     */
    public function list_arp($search = null){

        return json_decode($this->call_api('resources/ip/arp/' . $search));

    }


}