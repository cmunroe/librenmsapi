<?php

namespace librenmsApi\manipulations;


class deviceGroups extends \librenmsApi\core {

    /**
     * Get device info/details for a whole group.
     *
     * @param string $group device group name.
     * @return object details
     */
    public function get_group_details($group){

        $dg = new \librenmsApi\deviceGroups;
        $d = new \librenmsApi\devices;

        $dg->set_api($this->site, $this->token);
        $d->set_api($this->site, $this->token);
    
        // Get Device Group.
        $devices = $dg->get_devices_by_group($group);

        // Create our array.
        $data = array();

        // Loop through each device.
        foreach($devices->devices as $device){

            // Get device data, and place in array.
            $data[] = $d->get_device($device->device_id);

        }

        // return or data.
        return $data;

    }


}