<?php

namespace librenmsApi\manipulations;

/**
 * Find the port_id and details by MAC address or IP address.
 */
class findPorts extends \librenmsApi\core {

    /**
     * Our haystack cache.
     * Grabbing the data for this process is long, and process intensive. 
     * So we cache the result.
     * 
     * Initially it is stored as false, and later stored as an array.
     * 
     * You can dump this haystack using the dumpHayStack(); method if you
     * want to save this haystack between iterations. You can reimport the 
     * haystack with importHayStack(array $hayStack) method;
     *
     * @var array 
     */
    private $hayStack = false;


    /**
     * Update the haystack from LibreNMS by grabbing and downloading
     * the entire fbd table stored in LibreNMS. This is an 
     * intensive task. The results are stored in $this->hayStack for 
     * reuse as long as this class stays initiated. 
     *
     * @return this chain.
     */
    public function update(){

        $switching = new \librenmsApi\switching;

        $switching->set_api($this->site, $this->token);

        $results = $switching->list_fdb();

        $macMapping = array();

        foreach($results->ports_fdb as $port){

            if(!array_key_exists($port->port_id, $macMapping)){

                $macMapping[$port->port_id] = array();

            }

            array_push($macMapping[$port->port_id], $port->mac_address);

        }

        if(count($macMapping) > 0){

            $this->hayStack = $macMapping;

        }


        return $this;

    }

    /**
     * Discover the MAC Address of a device by IP.
     *
     * @param string $ip address.
     * @return string mac address / boolean false if errored.
     */
    public function ip2mac(string $ip){

        $arp = new \librenmsApi\arp;

        $arp->set_api($this->site, $this->token);

        $results = $arp->list_arp($ip);

        if($results->status == "ok"){

            foreach($results->arp as $item){

                if($item->ipv4_address == $ip AND $item->mac_address !== null){

                    return $item->mac_address;

                }

            }
        
        }

        return false;

    }

    /**
     * Lookup a Port_ID by IP Address. 
     * 
     * This is done by converting the IP address into a MAC Address.
     *
     * @param string $ip address
     * @return array results / boolean false if issue.
     */
    public function ipLookup(string $ip){

        $mac = $this->ip2mac($ip);

        if($mac !== false){

            return $this->macLookupPort($mac);

        }

        return false;

    }

    /**
     * Clean up MAC Address inputs. 
     * 
     * We need the mac address to be a string with no special 
     * characters like :, -, et cetera. These are commonly used 
     * in splitting up the mac address by 2 character sets. However,
     * libreNMS doesn't support mac addresses in this format.
     *
     * @param string $mac address
     * @return string mac address without special characters.
     */
    public function macCleanup(string $mac){

        $mac = str_replace(':', '', $mac);
        $mac = str_replace('-', '', $mac);
        $mac = str_replace(' ', '', $mac);

        return $mac;

    }

    /**
     * Lookup Port_id by MAC Address. 
     * 
     * If the haystack isn't initialized, we initialize it.
     * This can take a lot of time to process.
     * 
     * After that, we search through the list by the count of mac
     * addresses per port. Going from smallest count to a larger
     * amount until we find your mac address listed. We then return this value
     * as a most likely case of your device being on this port.
     * 
     * To make this as accurate as possible, we start from a count of 1
     * and increase. However, if we find a multiple ports on the same 
     * count. You should probably ignore it.
     * 
     * Multiple mac addresses on one port usually occur from small desktop 
     * switches and other devices. Phones are another possibility.
     * 
     * 
     * @param string $mac
     * @param intiger $maxSearch set the max count you wish to go up to
     * for searching for a device. The default is 10.
     * @return array Ports that are likely to have your device.
     */
    public function macLookupPort(string $mac, int $maxSearch = 10){

        $mac = $this->macCleanup($mac);

        if($this->hayStack === false){

            $this->update();

        }

        $needles = array();

        for($i = 1; $i <= 10; $i++){

            foreach($this->hayStack as $port => $stalk){

                if(count($stalk) == $i){

                    foreach($stalk as $sliver){

                        if($sliver == $mac){

                            $needles['devicesOnPort'] = $i;

                            if(!array_key_exists('ports', $needles)){

                                $needles['ports'] = array();

                            }

                            $needles['ports'][] = $port;

                            $needles['count'] = count($needles['ports']);

                        }

                    }
            
            
                }
            
            }

            if(array_key_exists('ports', $needles) && count($needles['ports']) > 0){

                return $needles;

            }

        }

        return false;

    }


    /**
     * After finding the port id 
     * you probably want to gather some info on the port.
     * 
     * This function will give you device and port info
     * combined.
     *
     * @param integer $port port_id as listed by Librenms.
     * @return object port details as well as device details.
     */
    public function portInfo(int $port){

        $response = array();

        $portsClass = new \librenmsApi\ports;

        $portsClass->set_api($this->site, $this->token);

        $portsInfo = $portsClass->get_port_info($port);

        if($portsInfo->status == "ok" && $portsInfo->port[0]->device_id !== null){

            $devicesClass = new \librenmsApi\devices;

            $devicesInfo = $devicesClass->set_api($this->site, $this->token)->get_device($portsInfo->port[0]->device_id);

            if($devicesInfo->status == "ok"){

                $response = (object) array_merge((array) $portsInfo->port[0], (array) $devicesInfo->devices[0]);

                return $response;

            }
            
        }

        return false;

    }

    
    /**
     * This allows you to dump the haystack for reuse.
     * 
     * You will need to use the importHayStack(array $hayStack); 
     * function to reimport the haystack. Make sure to still set
     * set_api(); otherwise it won't be able to use many of the 
     * functions.
     *
     * @return array hayStack.
     */
    public function dumpHayStack(){


        return $this->hayStack;

    }

    /**
     * Import our haystack. This can be used for caching
     * or resuse of assets already downloaded. Make sure
     * to still update the haystack on a regular basis.
     * Otherwise, data will be stale and not as accurate.
     *
     * @param array $hayStack
     * @return this chain.
     */
    public function importHayStack(array $hayStack){

        $this->hayStack = $hayStack;

        return $this;

    }

}