<?php

namespace librenmsApi;

class devices extends core {

    /**
     * Get Device info/details.
     *
     * @param string $hostname hostname can be either the device hostname or id
     * @return object returns the array of the device.
     */
    public function get_device(string $hostname){

        return json_decode($this->call_api("devices/" . $hostname));

    }

    /**
     * Delete a given device.
     *
     * @param string $hostname hostname can be either the device hostname or id
     * @return object
     */
    public function del_device(string $hostname){

        return json_decode($this->call_api("devices/" . $hostname, 'DELETE'));

    }
    
    /**
     * Get a list of available graphs for a device, this does not include ports.
     *
     * @param string $hostname hostname can be either the device hostname or id
     * @return object
     */
    public function get_graphs(string $hostname){

        return json_decode($this->call_api("devices/" . $hostname . '/graphs'));

    }

    /**
     * This function allows to do three things:
     *  * Get a list of overall health graphs available.
     *  * Get a list of health graphs based on provided class.
     *  * Get the health sensors information based on ID.
     *
     * @param string $hostname can be either the device hostname or id
     * @param string $type (optional) is health type / sensor class
     * @param integer $sensor_id (optional) is the sensor id to retrieve specific information. 
     * @return object
     */
    public function list_available_health_graphs(string $hostname, string $type = null, int $sensor_id = null){


        return json_decode($this->call_api("devices/" . $hostname . '/health/' . $type . '/' . $sensor_id));


    }

    /**
     * This function allows to do three things:
     *  * Get a list of overall wireless graphs available.
     *  * Get a list of wireless graphs based on provided class.
     *  * Get the wireless sensors information based on ID.
     *
     * @param string $hostname can be either the device hostname or id
     * @param string $type (optional) is health type / sensor class
     * @param integer $sensor_id (optional) is the sensor id to retrieve specific information. 
     * @return object
     */
    public function list_available_wireless_graphs(string $hostname, string $type = null, int $sensor_id = null){

        return json_decode($this->call_api("devices/" . $hostname . '/wireless/' . $type . '/' . $sensor_id));

    }

    /**
     * Get a particular health class graph for a device, if you provide a sensor_id as 
     * well then a single sensor graph will be provided.
     * If no sensor_id value is provided then you will be sent a stacked sensor graph.
     *
     * @param string $hostname can be either the device hostname or id
     * @param string $type is the name of the health graph as returned by list_available_health_graphs
     * @param integer $sensor_id (optional) restricts the graph to return a particular health sensor graph
     * @return string image
     */
    public function get_health_graph(string $hostname, string $type = null, int $sensor_id = null){

        return json_decode($this->call_api("devices/" . $hostname . '/graphs/health/' . $type . '/' . $sensor_id));

    }

    /**
     * Get a particular wireless class graph for a device, if you provide a sensor_id as 
     * well then a single sensor graph will be provided. 
     * If no sensor_id value is provided then you will be sent a stacked wireless graph.
     *
     * @param string $hostname can be either the device hostname or id
     * @param string $type  is the name of the wireless graph as returned by list_available_wireless_graphs
     * @param integer $sensor_id (optional) restricts the graph to return a particular wireless sensor graph.
     * @return string image
     */
    public function get_wireless_graph(string $hostname, string $type = null, int $sensor_id = null){

        return json_decode($this->call_api("devices/" . $hostname . '/graphs/wireless/' . $type . '/' . $sensor_id));

    }

    /**
     * Get a specific graph for a device, this does not include ports.
     *
     * @param string $hostname can be either the device hostname or id
     * @param string $type is the type of graph you want, use [get_graphs](#function-get_graphs to see the graphs available. Defaults to device uptime.
     * @param string $from This is the date you would like the graph to start - See http://oss.oetiker.ch/rrdtool/doc/rrdgraph.en.html for more information. !!TODO!! (( No Documentation ))
     * @param string $to This is the date you would like the graph to end - See http://oss.oetiker.ch/rrdtool/doc/rrdgraph.en.html for more information. !!TODO!! (( No Documentation ))
     * @param integer $width The graph width, defaults to 1075. !!TODO!! (( No Documentation ))
     * @param integer $height The graph height, defaults to 300. !!TODO!! (( No Documentation ))
     * @param string $output Set how the graph should be outputted (base64, display), defaults to display. !!TODO!! (( No Documentation ))
     * @return string image
     */
    public function get_graph_generic_by_hostname(string $hostname, string $type = 'uptime', string $from = null, string $to = null, int $width = 1075, int $height = 300, string $output = 'display'){

        return json_decode($this->call_api("devices/" . $hostname . '/' . $type));

    }
    
    /**
     * Get a list of ports for a particular device.
     *
     * @param string $hostname can be either the device hostname or id 
     * @param string $columns Comma separated list of columns you want returned. !!TODO!! (( No Documentation ))
     * @return object
     */
    public function get_port_graphs(string $hostname, string $columns = null){

        return json_decode($this->call_api("devices/" . $hostname . '/ports'));

    }

    /**
     * Get a list of FDB entries associated with a device.
     *
     * @param string $hostname can be either the device hostname or id
     * @return object 
     */
    public function get_device_fdb(string $hostname){

        return json_decode($this->call_api("devices/" . $hostname . '/fdb'));

    }

    /**
     * Get a list of IP addresses (v4 and v6) associated with a device.
     *
     * @param string $hostname can be either the device hostname or id
     * @return object 
     */
    public function get_device_ip_addresses(string $hostname){

        return json_decode($this->call_api("devices/" . $hostname . '/ip'));

    }

    /**
     * Get a list of port mappings for a device. This is useful for showing physical ports that are in a virtual port-channel.
     *
     * @param string $hostname can be either the device hostname or id
     * @param boolean $valid_mappings Filter the result by only showing valid mappings ("0" values not shown).
     * @return object 
     */
    public function get_port_stack(string $hostname, bool $valid_mappings = false){

        if($valid_mappings){

            return json_decode($this->call_api("devices/" . $hostname . '/port_stack?valid_mappings'));

        }

        return json_decode($this->call_api("devices/" . $hostname . '/port_stack'));

    }

    /**
     * Get a list of components for a particular device.
     *
     * @param string $hostname can be either the device hostname or id
     * @param string $type !!TODO!! (( No Documentation ))
     * @param string $id !!TODO!! (( No Documentation ))
     * @param string $label !!TODO!! (( No Documentation ))
     * @param integer $status !!TODO!! (( No Documentation ))
     * @param integer $disabled !!TODO!! (( No Documentation ))
     * @param string $ignore !!TODO!! (( No Documentation ))
     * @return object 
     */
    public function get_components(string $hostname){

        return json_decode($this->call_api("devices/" . $hostname . '/components'));

    }

    /**
     * Create a new component of a type on a particular device.
     * https://docs.librenms.org/API/Devices/#add_components
     *
     * @param string $hostname can be either the device hostname or id
     * @param string $type is the type of component to add
     * @param array $content Must Include 'type'.
     * @return object
     */
    public function add_components(string $hostname, string $type, array $content = null){

        $content = json_encode($content);

        return json_decode($this->call_api("devices/" . $hostname . '/components/' . $type, 'POST', $content));

    }

    /**
     * Edit an existing component on a particular device.
     * https://docs.librenms.org/API/Devices/#edit_components
     *
     * @param string $hostname  can be either the device hostname or id
     * @param array $content Must Include 'type'.
     * @return void
     */
    public function edit_components(string $hostname, array $content){

        $content = json_encode($content);

        return json_decode($this->call_api("devices/" . $hostname . '/components', 'PUT', $content));

    }

    /**
     * Delete an existing component on a particular device.
     *
     * @param string $hostname can be either the device hostname or id
     * @param integer $component is the component ID to be deleted.
     * @return object 
     */
    public function delete_components(string $hostname, int $component){

        return json_decode($this->call_api("devices/" . $hostname . '/components/' . $component, 'DELETE'));

    }

    /**
     * Undocumented function
     *
     * @param string $hostname can be either the device hostname or id
     * @param string $ifname can be any of the interface names for the device which can be obtained using
     * get_port_graphs. Please ensure that the ifname is urlencoded if it needs to be (i.e Gi0/1/0 would need to be urlencoded.
     * @param string $columns Comma separated list of columns you want returned. !!TODO!! (( No Documentation ))
     * @return object
     */
    public function get_port_stats_by_port_hostname(string $hostname, string $ifname, string $columns = null){

        return json_decode($this->call_api("devices/" . $hostname . '/ports/' . $ifname));

    }

    /**
     * Get a graph of a port for a particular device.
     *
     * @param string $hostname can be either the device hostname or id
     * @param string $ifname can be any of the interface names for the device which can be obtained using get_port_graphs.
     * Please ensure that the ifname is urlencoded if it needs to be (i.e Gi0/1/0 would need to be urlencoded.
     * @param string $type is the port type you want the graph for, you can request a list of ports for a device with get_port_graphs.
     * @param string $from This is the date you would like the graph to start - See http://oss.oetiker.ch/rrdtool/doc/rrdgraph.en.html for more information. !!TODO!! (( No Documentation ))
     * @param string $to This is the date you would like the graph to end - See http://oss.oetiker.ch/rrdtool/doc/rrdgraph.en.html for more information. !!TODO!! (( No Documentation ))
     * @param integer $width The graph width, defaults to 1075. !!TODO!! (( No Documentation ))
     * @param integer $height The graph height, defaults to 300. !!TODO!! (( No Documentation ))
     * @param string $ifDescr  If this is set to true then we will use ifDescr to lookup the port instead of ifName. Pass the ifDescr value you want to search as you would ifName. !!TODO!! (( No Documentation ))
     * @return void
     */
    public function get_graph_by_port_hostname(string $hostname, string $ifname, string $type){

        return json_decode($this->call_api("devices/" . $hostname . '/ports/' . $ifname . "/" . $type));

    }

    /**
     * Return a list of locations.
     *
     * @return void
     */
    public function list_locations(){

        return json_decode($this->call_api("resources/locations"));

    }

    /**
     * Get a list of all Sensors.
     *
     * @return object
     */
    public function list_sensors(){

        return json_decode($this->call_api("resources/sensors"));
    }

    /**
     * Return a list of devices.
     *
     * @param array $search
     * order: How to order the output, default is by hostname. Can be prepended by DESC or ASC to change the order.
     * type: can be one of the following to filter or search by:
     * all: All devices
     * active: Only not ignored and not disabled devices
     * ignored: Only ignored devices
     * up: Only devices that are up
     * down: Only devices that are down
     * disabled: Disabled devices
     * os: search by os type
     * mac: search by mac address
     * ipv4: search by IPv4 address
     * ipv6: search by IPv6 address (compressed or uncompressed)
     * location: search by location
     * query: If searching by, then this will be used as the input.
     * @return object
     */
    public function list_devices(array $search = array()){

        return json_decode($this->call_api("devices?" . http_build_query($search)));

    }

    /**
     * Add a new device.
     *
     * @param array $setting
     * hostname: device hostname
     * port: SNMP port (defaults to port defined in config).
     * transport: SNMP protocol (defaults to transport defined in config).
     * version: SNMP version to use, v1, v2c or v3. Defaults to v2c.
     * poller_group: This is the poller_group id used for distributed poller setup. Defaults to 0.
     * force_add: Force the device to be added regardless of it being able to respond to snmp or icmp.
     * For SNMP v1 or v2c
     * 
     * community: Required for SNMP v1 or v2c.
     * For SNMP v3
     * 
     * authlevel: SNMP authlevel (noAuthNoPriv, authNoPriv, authPriv).
     * authname: SNMP Auth username
     * authpass: SNMP Auth password
     * authalgo: SNMP Auth algorithm (MD5, SHA)
     * cryptopass: SNMP Crypto Password
     * cryptoalgo: SNMP Crypto algorithm (AES, DES)
     * For ICMP only
     * 
     * snmp_disable: Boolean, set to true for ICMP only.
     * os: OS short name for the device (defaults to ping).
     * hardware: Device hardware.
     * @return object
     */
    public function add_device(array $setting){

        $content = json_encode($settings);

        return json_decode($this->call_api("devices", 'POST', $content));

    }

    /**
     * List devices for use with Oxidized. If you have group support enabled then a group will also be returned based on your config.
     * 
     * LibreNMS will automatically map the OS to the Oxidized model name if they don't match.
     *
     * @param string $hostname
     * @return void
     */
    public function list_oxidized(string $hostname = null){

        return json_decode($this->call_api("oxidized/" . $hostname));

    }

    /**
     * Update devices field in the database.
     *
     * @param string $hostname can be either the device hostname or id
     * @param array $input
     * field: The column name within the database (can be an array of fields)
     * data: The data to update the column with (can be an array of data))
     * @return object
     */
    public function update_device_field(string $hostname, array $input){

        $input = json_encode($input);

        return json_decode($this->call_api("devices/" . $hostname, 'PATCH', $input));

    }

    /**
     * Rename device.
     *
     * @param string $old hostname
     * @param string $new hostname
     * @return object
     */
    public function rename_device(string $old, string $new){

        return json_decode($this->call_api("devices/" . $old . '/rename/' . $new, 'PATCH'));

    }

    /**
     * List the device groups that a device is matched on.
     *
     * @param string $hostname can be either the device hostname or id
     * @return object
     */
    public function get_device_groups(string $hostname){

        return json_decode($this->call_api("devices/" . $hostname . "/groups"));

    }

}