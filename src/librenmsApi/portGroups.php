<?php

namespace librenmsApi;

class portGroups extends core {

    /**
     * Get the graph based on the group type.
     *
     * @param string $group is the type of port group graph you want, I.e Transit, Peering, etc. You can specify multiple types comma separated.
     * @param array $option
     *  - from: This is the date you would like the graph to start - See http://oss.oetiker.ch/rrdtool/doc/rrdgraph.en.html for more information.
     *  - to: This is the date you would like the graph to end - See http://oss.oetiker.ch/rrdtool/doc/rrdgraph.en.html for more information.
     *  - width: The graph width, defaults to 1075.
     *  - height: The graph height, defaults to 300.
     * @return string image
     */
    public function get_graph_by_portgroup(string $group, array $option = array()){

        return json_decode($this->call_api("portgroups/" . $group . "?" . http_build_query($options)));
        
    }

    /**
     * Get the graph based on the multiple port id separated by commas.
     *
     * @param string $id is a comma separated list of port ids you want, I.e 1,2,3,4, etc. You can specify multiple IDs comma separated.
     * @param array $option
     *  - from: This is the date you would like the graph to start - See http://oss.oetiker.ch/rrdtool/doc/rrdgraph.en.html for more information.
     *  - to: This is the date you would like the graph to end - See http://oss.oetiker.ch/rrdtool/doc/rrdgraph.en.html for more information.
     *  - width: The graph width, defaults to 1075.
     *  - height: The graph height, defaults to 300.
     * @return string image
     */
    public function get_graph_by_portgroup_multiport_bits(string $id, array $options = array()){

        return json_decode($this->call_api("portgroups/multiport/bits/" . $id . "?" . http_build_query($options)));

    }

}