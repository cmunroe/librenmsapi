<?php

namespace librenmsApi;

class inventory extends core {

    /**
     * Retrieve the inventory for a device. If you call this without any parameters then you will only get part of the inventory. 
     * This is because a lot of devices nest each component,
     * for instance you may initially have the chassis, within this the ports - 1 being an sfp cage, 
     * then the sfp itself. The way this API call is designed is to enable a recursive lookup. 
     * The first call will retrieve the root entry, included within this response will be entPhysicalIndex, 
     * you can then call for entPhysicalContainedIn which will then return the next layer of results.
     *
     * @param string $hostname hostname can be either the device hostname or the device id
     * @param string $entPhysicalClass This is used to restrict the class of the inventory, 
     * for example you can specify chassis to only return items in the inventory that are labelled as chassis.
     * @param integer $entPhysicalContainedIn This is used to retrieve items within the inventory assigned
     * to a previous component, for example specifying the chassis (entPhysicalIndex) will retrieve all items where the chassis is the parent.
     * @return object
     */
    public function get_inventory(string $hostname = null, string $entPhysicalClass = null, int $entPhysicalContainedIn = null){

        return json_decode($this->call_api('inventory/' . $hostname . '?entPhysicalClass=' . $entPhysicalClass . '&entPhysicalContainedIn' . $entPhysicalContainedIn));

    }

}